package carrental;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import entity.Car;
import facade.IFacade;

@RestController
@RequestMapping("/cars")
public class CarController {

	@Autowired
	private IFacade facade;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Car getCar(@PathVariable long id) {
		return facade.getCar(id);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Car createCar(@RequestBody final Car car) {
		facade.createCar(car);
		return car;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteCar(@PathVariable long id) {
		facade.removeCar(id);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void updateCar(@PathVariable long id, @RequestBody Car car) {
		facade.updateCar(id, car);
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getCars(Map<String, Object> model) {
		List<Car> cars = facade.getCars();
		model.put("Cars", cars);
		return "cars";
	}

}
