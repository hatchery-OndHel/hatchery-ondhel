package carrental;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import dao.CarDao;
import entity.Car;
import service.CarService;

@RestController
@RequestMapping("/cars")
public class CarController {

	@Autowired
	private CarService cd;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Car getCar(@PathVariable long id) {
		return cd.getCar(id);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Car createCar(@RequestBody final Car car) {
		cd.createCar(car);
		return car;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteCar(@PathVariable long id) {
		cd.removeCar(id);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void updateCar(@PathVariable long id, @RequestBody Car car) {
		cd.updateCar(id, car);
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Car> getCars() {
		return cd.getCars();
	}

}
