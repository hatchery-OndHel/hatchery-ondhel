<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@page
	import="entity.*, java.util.*"%>
<body>
<head>
<style>
body {
	background-color: #70B670;
}
hr {
display: block; height: 1px; border: 0; border-top: 1px solid #000; margin: 1em 0; padding: 0;
}
</style>
</head>
<div>
	<h2>List of Agencies</h2>

	<table title="List Of Agencies" border="1">
		<tr>
			<th>ID</th>
			<th>City</th>
			<th>Street</th>
			<th>Street number</th>
			<th>Cars' IDs</th>
		</tr>

		<%
			List<Agency> agencies = (List) request.getAttribute("Agencies");
			for (Agency agency : agencies) {
				out.println("<tr>");
				out.println("<td>" + agency.getId() + "</td>");
				out.println("<td>" + agency.getCity() + "</td>");
				out.println("<td>" + agency.getStreet() + "</td>");
				out.println("<td>" + agency.getStreetNumber() + "</td>");
				out.print("<td>");
				List<Car> cars = agency.getCars();
				for (Car car :cars){
					out.print(car.getId() + " ");
				}
				out.print("</td>");
				out.println("</tr>");
			}
		%>

	</table>

</div>
</body>