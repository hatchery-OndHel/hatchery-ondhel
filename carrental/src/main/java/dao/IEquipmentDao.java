package dao;

import java.util.List;

import org.springframework.stereotype.Component;

import entity.Equipment;

@Component
public interface IEquipmentDao {
	
	public void createEquipment(Equipment equipment);
	public Equipment getEquipment(long id);
	public List<Equipment> getEquipments();
	public void removeEquipment(long id);
	public void updateEquipment(long id, Equipment equipment);
	
}
