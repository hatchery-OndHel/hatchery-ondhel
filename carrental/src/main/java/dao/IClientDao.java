package dao;

import java.util.List;

import org.springframework.stereotype.Component;

import entity.Client;

@Component
public interface IClientDao {
	
	public void createClient(Client client);
	public Client getClient(long id);
	public List<Client> getClients();
	public void removeClient(long id);
	public void updateClient(long id, Client client);
}
