package dao;

import java.text.ParseException;
import java.util.List;

import org.springframework.stereotype.Component;

import entity.Reservation;

@Component
public interface IReservationDao {
	
	public void createReservation(Reservation reservation);
	public Reservation getReservation(long id);
	public void removeReservation(long id);
	public void updateReservation(long id, Reservation reservation)throws ParseException ;
	public List<Reservation> getReservations();
	public void correct(long id);
}
