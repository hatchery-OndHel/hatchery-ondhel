package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import entity.Client;

@Repository
public class ClientDao {

	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public void createClient(Client client){
		if (client.getId() != 0) client.setId(0);
		em.persist(client);
	}
	
	@Transactional
	public Client getClient(long id){
		Query q = em.createQuery("SELECT c FROM Client c WHERE id=:id");
		q.setParameter("id", id);
		Client c = (Client) q.getSingleResult();
		Hibernate.initialize(c.getReservation());
		return c;
	}
	
	@Transactional
	public List<Client> getClients() {
		Query q = em.createQuery("SELECT c FROM Client c");
		return q.getResultList();
	}
	
	@Transactional
	public void removeClient(long id){
		Query q = em.createQuery("SELECT c FROM Client c WHERE id=:id");
		q.setParameter("id", id);
		em.remove(q.getSingleResult());
	}
	
	@Transactional
	public void updateClient(long id, Client client){
		Query q = em.createQuery("SELECT c FROM Client c WHERE id=:id");
		q.setParameter("id", id);
		Client client1 = (Client) q.getSingleResult();
		
		if (client.getName() != null){
			client1.setName(client.getName());
		}
		if (client.getSurname() != null){
			client1.setSurname(client.getSurname());
		}
		if (client.getBirthNumber() != null){
			client1.setBirthNumber(client.getBirthNumber());
		}		
		if (client.getPhoneNumber() != null){
			client1.setPhoneNumber(client.getPhoneNumber());
		}
		if (client.getEmail() != null){
			client1.setEmail(client.getEmail());
		}
		
		em.merge(client1);
		em.persist(client1);
	}

}
