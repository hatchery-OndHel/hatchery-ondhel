package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import entity.Equipment;



@Repository
public class EquipmentDao {

	@PersistenceContext
	private EntityManager em;

	@Transactional
	public void createEquipment(Equipment equipment){
		if (equipment.getId() != 0) equipment.setId(0);
		em.persist(equipment);
	
	}
	
	@Transactional
	public Equipment getEquipment(long id){
		Query q = em.createQuery("SELECT e FROM Equipment e WHERE id=:id");
		q.setParameter("id", id);
		Equipment equipment = (Equipment) q.getSingleResult();
		return equipment;
	}
	
	@Transactional
	public List<Equipment> getEquipments() {
		Query q = em.createQuery("SELECT e FROM Equipment e");
		return q.getResultList();
	}
	
	@Transactional
	public void removeEquipment(long id){
		Query q = em.createQuery("SELECT e FROM Equipment e WHERE id=:id");
		q.setParameter("id", id);
		em.remove(q.getSingleResult());
	}
	
	@Transactional
	public void updateEquipment(long id, Equipment equipment){
		Query q = em.createQuery("SELECT e FROM Equipment e WHERE id=:id");
		q.setParameter("id", id);
		Equipment equipment1 = (Equipment) q.getSingleResult();
		
		if (equipment.getCars() != null){
			equipment1.setCars(equipment.getCars());
		}
		if (equipment.getExtraFee() != null){
			equipment1.setExtraFee(equipment.getExtraFee());
		}
		if (equipment.getType() != null){
			equipment1.setType(equipment.getType());
		}
		
		em.merge(equipment1);
		em.persist(equipment1);
	}
}
