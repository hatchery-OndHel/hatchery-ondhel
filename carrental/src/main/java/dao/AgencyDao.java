package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import entity.Agency;
import entity.Car;

@Repository
public class AgencyDao {

	@PersistenceContext
	private EntityManager em;

	@Transactional
	public void createAgency(Agency agency) {
		if (agency.getId() != 0)
			agency.setId(0);
		em.persist(agency);
	}

	@Transactional
	public Agency getAgency(long id) {
		Query q = em.createQuery("SELECT a FROM Agency a WHERE id=:id");
		q.setParameter("id", id);
		Agency agency = (Agency) q.getSingleResult();
		Hibernate.initialize(agency.getCars());
		return agency;
	}
	
	@Transactional
	public List<Agency> getAgencies(){		
		Query q = em.createQuery("SELECT a FROM Agency a");
		List<Agency> agencies = q.getResultList();
		for (Agency a : agencies) {
			Hibernate.initialize(a.getCars());
		}
		return agencies;
	}

	@Transactional
	public void removeAgency(long id) {
		Query q = em.createQuery("SELECT a FROM Agency a WHERE id=:id");
		q.setParameter("id", id);
		em.remove(q.getSingleResult());
	}

	@Transactional
	public void updateAgency(long id, Agency agency) {
		Query q = em.createQuery("SELECT a FROM Agency a WHERE id=:id");
		q.setParameter("id", id);
		Agency agency1 = (Agency) q.getSingleResult();

		if (agency.getCars() != null) {
			agency1.setCars(agency.getCars());
		}
		if (agency.getCity() != null) {
			agency1.setCity(agency.getCity());
		}
		if (agency.getStreet() != null) {
			agency1.setStreet(agency1.getStreet());
		}
		if (agency.getStreetNumber() != null) {
			agency1.setStreetNumber(agency.getStreetNumber());
		}
		em.merge(agency1);
		em.persist(agency1);
	}

	@Transactional
	public List<Agency> getAgencies() {
		Query q = em.createQuery("SELECT a FROM Agency a");
		List<Agency> agencies = q.getResultList();
		for (Agency a : agencies) {
			Hibernate.initialize(a.getCars());
		}
		return agencies;

	}
}
