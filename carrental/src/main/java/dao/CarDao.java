package dao;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import entity.Car;


@Repository
public class CarDao {

	@PersistenceContext
	private EntityManager em;

	@Transactional
	public void createCar(Car car){
		if (car.getId() != 0) car.setId(0);
		em.persist(car);
	
	}
	
	@Transactional
	public Car getCar(long id){
		Query q = em.createQuery("SELECT c FROM Car c WHERE id=:id");
		q.setParameter("id", id);
		Car c = (Car) q.getSingleResult();
		return c;
	}
	
	@Transactional
	public void removeCar(long id){
		Query q = em.createQuery("SELECT c FROM Car c WHERE id=:id");
		q.setParameter("id", id);
		em.remove(q.getSingleResult());
	}
	
	@Transactional
	public List<Car> getCars(){
		Query q = em.createQuery("SELECT c FROM Car c");
		return q.getResultList();
	}
	
	@Transactional
	public void updateCar(long id, Car car){
		Query q = em.createQuery("SELECT c FROM Car c WHERE id=:id");
		q.setParameter("id", id);
		Car car1 = (Car) q.getSingleResult();
		
		if (car.getAgency() != null){
			car1.setAgency(car.getAgency());
		}
		if (car.getColor() != null){
			car1.setColor(car.getColor());
		}
		if (car.getModel() != null){
			car1.setModel(car.getModel());
		}
		if (car.getPricePerDay() != 0){
			car1.setPricePerDay(car.getPricePerDay());
		}
		if (car.getSpz() != null){
			car1.setSpz(car.getSpz());
		}
		if (car.getType()!= null){
			car1.setType(car.getType());
		}
		
		em.merge(car1);
		em.persist(car1);
	}
}
