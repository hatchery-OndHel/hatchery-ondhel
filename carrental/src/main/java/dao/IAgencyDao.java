package dao;

import java.util.List;

import org.springframework.stereotype.Component;

import entity.Agency;

@Component
public interface IAgencyDao {
	
	public void createAgency(Agency agency);
	public Agency getAgency(long id);
	public void removeAgency(long id);
	public void updateAgency(long id, Agency agency);
	public List<Agency> getAgencies();
}
