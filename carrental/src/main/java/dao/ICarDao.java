package dao;

import java.util.List;

import org.springframework.stereotype.Component;

import entity.Car;

@Component
public interface ICarDao {
	
	public void createCar(Car car);
	public Car getCar(long id);
	public void removeCar(long id);
	public List<Car> getCars();
	public void updateCar(long id, Car car);
	
}
