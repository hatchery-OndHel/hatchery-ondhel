package facade;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import entity.Agency;
import entity.Car;
import entity.Client;
import entity.Equipment;
import entity.Reservation;
import service.AgencyService;
import service.CarService;
import service.ClientService;
import service.EquipmentService;
import service.ReservationService;

@Component
public class Facade implements IFacade {

	@Autowired
	private EquipmentService es;
	@Autowired
	private CarService cs;
	@Autowired
	private AgencyService as;
	@Autowired
	private ReservationService rs;
	@Autowired
	private ClientService cls;

	// Equipments
	public List<Equipment> getEquipments() {
		return es.getEquipments();
	}

	public Equipment getEquipment(long id) {
		return es.getEquipment(id);
	}

	public void updateEquipment(long id, Equipment equipment) {
		es.updateEquipment(id, equipment);
	}

	public void createEquipment(Equipment equipment) {
		es.createEquipment(equipment);
	}
	public void removeEquipment(long id) {
		es.removeEquipment(id);
	}

	// Agencies
	public List<Agency> getAgencies() {
		return as.getAgencies();
	}

	public Agency getAgency(long id) {
		return as.getAgency(id);
	}

	public void updateAgency(long id, Agency agency) {
		as.updateAgency(id, agency);

	}

	public void createAgency(Agency agency) {
		as.createAgency(agency);
	}
	
	public void removeAgency(long id) {
		as.removeAgency(id);
	}

	// Cars
	public List<Car> getCars() {
		return cs.getCars();
	}

	public Car getCar(long id) {
		return cs.getCar(id);
	}

	public void updateCar(long id, Car car) {
		cs.updateCar(id, car);
	}

	public void createCar(Car car) {
		cs.createCar(car);
	}
	public void removeCar(long id) {
		cs.removeCar(id);
	}
	

	// Clients
	public List<Client> getClients() {
		return cls.getClients();
	}

	public Client getClient(long id) {
		return cls.getClient(id);
	}

	public void updateClient(long id, Client client) {
		cls.updateClient(id, client);
	}

	public void createClient(Client client) {
		cls.createClient(client);
	}
	
	public void removeClient(long id) {
		cls.removeClient(id);
	}

	// Reservations
	public List<Reservation> getReservations() {
		return rs.getReservations();
	}

	public Reservation getReservation(Long id) {
		return rs.getReservation(id);
	}

	public void updateReservation(long id, Reservation reservation) throws ParseException {
		rs.updateReservation(id, reservation);

	}

	public void createReservation(Reservation reservation) {
		rs.createReservation(reservation);

	}
	
	public void removeReservation(long id){
		rs.removeReservation(id);
	}

}