package facade;

import java.text.ParseException;
import java.util.List;

import org.springframework.stereotype.Component;

import entity.Agency;
import entity.Car;
import entity.Client;
import entity.Equipment;
import entity.Reservation;

@Component
public interface IFacade {
	
	//equipment
	public List<Equipment> getEquipments();
	public Equipment getEquipment(long id);
	public void updateEquipment (long id, Equipment equipment);
	public void createEquipment (Equipment equipment);
	public void removeEquipment (long id);
	
	//agency
	public List<Agency> getAgencies();
	public Agency getAgency(long id);
	public void updateAgency (long id,Agency agency);
	public void createAgency (Agency agency);
	public void removeAgency (long id);
	
	//car
	public List<Car> getCars();
	public Car getCar(long id);
	public void updateCar (long id, Car car);
	public void createCar (Car car);
	public void removeCar (long id);
	
	//client
	public List<Client> getClients();
	public Client getClient(long id);
	public void updateClient (long id, Client client);
	public void createClient (Client client);
	public void removeClient (long id);
	
	//reservation
	public List<Reservation> getReservations();
	public Reservation getReservation(Long id);
	public void updateReservation (long id, Reservation reservation) throws ParseException;
	public void createReservation (Reservation reservation);
	public void removeReservation (long id);
}
