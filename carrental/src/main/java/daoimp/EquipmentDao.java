package daoimp;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dao.IEquipmentDao;
import entity.Equipment;


@Transactional
@Repository
public class EquipmentDao implements IEquipmentDao{

	@PersistenceContext
	private EntityManager em;

	
	public void createEquipment(Equipment equipment){
		if (equipment.getId() != 0) equipment.setId(0);
		em.persist(equipment);
	
	}
	
	public Equipment getEquipment(long id){
		Query q = em.createQuery("SELECT e FROM Equipment e WHERE id=:id");
		q.setParameter("id", id);
		Equipment equipment = (Equipment) q.getSingleResult();
		return equipment;
	}
	
	public List<Equipment> getEquipments() {
		Query q = em.createQuery("SELECT e FROM Equipment e");
		return q.getResultList();
	}
	
	public void removeEquipment(long id){
		Query q = em.createQuery("SELECT e FROM Equipment e WHERE id=:id");
		q.setParameter("id", id);
		em.remove(q.getSingleResult());
	}
	
	public void updateEquipment(long id, Equipment equipment){
		Query q = em.createQuery("SELECT e FROM Equipment e WHERE id=:id");
		q.setParameter("id", id);
		Equipment equipment1 = (Equipment) q.getSingleResult();
		
		if (equipment.getCars() != null){
			equipment1.setCars(equipment.getCars());
		}
		if (equipment.getExtraFee() != null){
			equipment1.setExtraFee(equipment.getExtraFee());
		}
		if (equipment.getType() != null){
			equipment1.setType(equipment.getType());
		}
		
		em.merge(equipment1);
		em.persist(equipment1);
	}
}
