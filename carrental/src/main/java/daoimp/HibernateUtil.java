package daoimp;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.List;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import entity.*;

public class HibernateUtil {

	public static void main(String[] arg) throws ParseException{
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		
		Session s =sf.openSession();
		Transaction t = s.beginTransaction();
		
		Car c = new Car("5B54444","Sedan","M3","blue",3000, null, null);
		List<Car> cars = new ArrayList<>();
		cars.add(c);
		
		Equipment e = new Equipment("Aircondition",3478,cars);
		List<Equipment> equipments = new ArrayList<>();
		equipments.add(e);
		
		c.setEquipments(equipments);
		
		Client cl = new Client("Jozef","Mrkvicka","940689/7744","0919180673","kajhdksj@gmail.com",null);
		
		DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
		
		Reservation res = new Reservation(cl,c,"Londyn","Glasgow",10, df.parse("12.06.2015"), df.parse("22.06.2015"));
		List<Reservation> reservations = new ArrayList<>();
		reservations.add(res);
		
		cl.setReservation(reservations);
		
		Agency ag = new Agency("Lodyn","Water bay street",15,cars);
		c.setAgency(ag);
		
		s.persist(e);
		s.persist(ag);
		s.persist(c);
		s.persist(cl);
		
		s.persist(res);
		s.persist(ag);
		
		t.commit();
		
		selectData(s);
		
		s.close();
		
		
	}
	
	private static void selectData (Session s){
//		Query q = s.createQuery("select agency from car where car.agency =:cagency");
//		q.setParameter("cagency", Long.valueOf(1));
//		List<Object> result = q.list();
//		for (Object res:result)
//		System.out.println(res.toString());
		
		String hql = "SELECT type FROM Car C WHERE C.color = :car_color";
		Query query = s.createQuery(hql);
		query.setParameter("car_color","blue");
		List<Object> results = query.list();
		for (Object res: results)
		System.out.println(res.toString());
		
	}
	
	
}
