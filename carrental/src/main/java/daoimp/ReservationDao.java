package daoimp;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dao.IReservationDao;
import entity.Car;
import entity.Reservation;

@Transactional
@Repository
public class ReservationDao implements IReservationDao{

	@PersistenceContext
	private EntityManager em;

	
	public void createReservation(Reservation reservation) {
		if (reservation.getId() != 0)
			reservation.setId(0);
		em.persist(reservation);
	}

	public Reservation getReservation(long id) {
		Query q = em.createQuery("SELECT r FROM Reservation r WHERE id=:id");
		q.setParameter("id", id);
		Reservation r = (Reservation) q.getSingleResult();
		Hibernate.initialize(r.getClient());
		Hibernate.initialize(r.getCar());
		return r;
	}

	public void removeReservation(long id) {
		Query q = em.createQuery("SELECT r FROM Reservation r WHERE id=:id");
		q.setParameter("id", id);
		em.remove(q.getSingleResult());
	}

	public void updateReservation(long id, Reservation reservation) throws ParseException {
		Query q = em.createQuery("SELECT r FROM Reservation r WHERE id=:id");
		q.setParameter("id", id);
		Reservation reservation1 = (Reservation) q.getSingleResult();

		if (reservation.getStartPlace() != null) {
			reservation1.setStartPlace(reservation.getStartPlace());
		}
		if (reservation.getReturnPlace() != null) {
			reservation1.setReturnPlace(reservation.getReturnPlace());
		}
		if (reservation.getDuration() != null) {
			reservation1.setDuration(reservation.getDuration());
		}
		if (reservation.getStartDate() != null) {
			reservation1.setStartDate(reservation.getStartDate());
		}
		if (reservation.getReturnDate() != null) {
			reservation1.setReturnDate(reservation.getReturnDate());
		}

		em.merge(reservation1);
		em.persist(reservation1);
	} // end of update

	public void correct(long id) {
		Query q = em.createQuery("SELECT car FROM Reservation r join r.car as car WHERE r.id=:id");
		q.setParameter("id", id);
		Car car = (Car) q.getSingleResult();

		Query q1 = em.createQuery("SELECT r FROM Reservation r WHERE id=:id");
		q1.setParameter("id", id);
		Reservation r = (Reservation) q1.getSingleResult();
		if (r.returnDateAsDate().before(new Date())) {
			car.setAvailable(true);
		} else {
			car.setAvailable(false);
			}
		em.merge(car);
		r.setCar(car);
		em.persist(r);
	}
	
	public List<Reservation> getReservations(){
		Query q = em.createQuery("SELECT r FROM Reservation r");
		return q.getResultList();
	}
}
