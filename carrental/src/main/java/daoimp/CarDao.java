package daoimp;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.StringUtils;

import dao.ICarDao;
import entity.Car;

@Transactional
@Repository
public class CarDao implements ICarDao{

	@PersistenceContext
	private EntityManager em;

	
	public void createCar(Car car){
		if (car.getId() != 0) car.setId(0);
		em.persist(car);
	
	}
	

	public Car getCar(long id){
		Query q = em.createQuery("SELECT c FROM Car c WHERE id=:id");
		q.setParameter("id", id);
		if (q.getResultList().size() == 0){
			return null;
		}
		Car c = (Car) q.getSingleResult();
		return c;
	}
	
	
	public void removeCar(long id){
		Query q = em.createQuery("SELECT c FROM Car c WHERE id=:id");
		q.setParameter("id", id);
		if (q.getResultList().size() == 0){
			return;
		}
		em.remove(q.getSingleResult());
	}
	
	
	public List<Car> getCars(){
		Query q = em.createQuery("SELECT c FROM Car c");
		return q.getResultList();
	}
	
	
	public void updateCar(long id, Car car){
		Query q = em.createQuery("SELECT c FROM Car c WHERE id=:id");
		q.setParameter("id", id);
		if (q.getResultList().size() == 0){
			return;	
		}
		Car car1 = (Car) q.getSingleResult();
		
		if (car.getAgency() != null){
			car1.setAgency(car.getAgency());
		}
		if (!StringUtils.isNullOrEmpty(car.getColor())){
			car1.setColor(car.getColor());
		}
		if (!StringUtils.isNullOrEmpty(car.getModel())){
			car1.setModel(car.getModel());
		}
		if (car.getPricePerDay() != null){
			car1.setPricePerDay(car.getPricePerDay());
		}
		if (!StringUtils.isNullOrEmpty(car.getSpz())){
			car1.setSpz(car.getSpz());
		}
		if (!StringUtils.isNullOrEmpty(car.getType())){
			car1.setType(car.getType());
		}
		
		em.merge(car1);
		em.persist(car1);
	}
}
