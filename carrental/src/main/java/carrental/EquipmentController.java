package carrental;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import entity.Equipment;
import facade.IFacade;


@RestController
@RequestMapping("/equipments")
public class EquipmentController {
	
	@Autowired
	private IFacade facade;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Equipment getEquipment(@PathVariable long id){
		return facade.getEquipment(id);
	}
	
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Equipment createEquipment(@RequestBody final Equipment equipment) {
		facade.createEquipment(equipment);
		return equipment;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteEquipment(@PathVariable long id){
		facade.removeEquipment(id);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void updateEquipment(@PathVariable long id,@RequestBody Equipment equipment){
		facade.updateEquipment(id,equipment);
	}
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Equipment> getEquipments() {
		return facade.getEquipments();
	}
}
