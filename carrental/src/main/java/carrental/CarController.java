package carrental;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import entity.Car;
import facade.IFacade;

@Controller
@RequestMapping("/cars")
public class CarController {

	@Autowired
	private IFacade facade;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Car getCar(@PathVariable long id) {
		return facade.getCar(id);
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String createCar(@ModelAttribute("NewCar") Car car, ModelMap model)  {
		facade.createCar(car);
		return "redirect:";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String deleteCar(@ModelAttribute("id") long id) {
		facade.removeCar(id);
		return "redirect:";
	}
	

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateCar(@ModelAttribute("NewCar1") Car car, ModelMap model) {
		facade.updateCar(car.getId(),car);
		return "redirect:";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView getCars(Map<String, Object> model) {
		List<Car> cars = facade.getCars();
		model.put("Cars", cars);
		model.put("NewCar", new Car());
		model.put("NewCar1", new Car());

		ModelAndView mv = new ModelAndView("cars");
		return mv;
	}

}
