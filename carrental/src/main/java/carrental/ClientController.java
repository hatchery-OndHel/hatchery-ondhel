package carrental;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import entity.Client;
import facade.IFacade;

@RestController
@RequestMapping("/clients")
public class ClientController {
	
	@Autowired
	private IFacade facade;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Client getClient(@PathVariable long id){
		return facade.getClient(id);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView getClients(Map<String, Object> model) {
		List<Client> clients = facade.getClients();
		model.put("Clients", clients);
		ModelAndView mv = new ModelAndView("clients");
		return mv;
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Client createClient(@RequestBody final Client client) {
		facade.createClient(client);
		return client;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteClient(@PathVariable long id){
		facade.removeClient(id);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void updateClient(@PathVariable long id,@RequestBody Client client){
		facade.updateClient(id,client);
	}
	
	
}
