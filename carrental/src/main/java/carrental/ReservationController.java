package carrental;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import entity.Reservation;
import facade.IFacade;

@RestController
@RequestMapping("/reservations")
public class ReservationController {

	@Autowired
	private IFacade facade;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Reservation getReservation(@PathVariable long id){
		return facade.getReservation(id);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Reservation createReservation(@RequestBody final Reservation reservation) {
		facade.createReservation(reservation);
		return reservation;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteReservation(@PathVariable long id){
		facade.removeReservation(id);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void updateReservation(@PathVariable long id,@RequestBody Reservation reservation) throws ParseException {
		facade.updateReservation(id,reservation);
	}
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Reservation> getReservations() {
		return facade.getReservations();
	}
	
}
