package carrental;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import entity.Agency;
import entity.Car;
import facade.IFacade;

@RestController
@RequestMapping("/agencies")
public class AgencyController {
	
	@Autowired
	private IFacade facade;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Agency getAgency(@PathVariable long id){
		return facade.getAgency(id);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Agency createAgency(@RequestBody final Agency agency) {
		facade.createAgency(agency);
		return agency;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteAgency(@PathVariable long id){
		facade.removeAgency(id);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void updateAgency(@PathVariable long id,@RequestBody Agency agency){
		facade.updateAgency(id,agency);
	}
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView getAgencies(Map<String, Object> model) {
		List<Agency> agencies = facade.getAgencies();
		model.put("Agencies", agencies);
		ModelAndView mv = new ModelAndView("agencies");
		return mv;
	}
	
}

