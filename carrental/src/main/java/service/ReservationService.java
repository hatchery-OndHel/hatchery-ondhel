package service;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import dao.IReservationDao;
import entity.Reservation;

@Component
public class ReservationService {
	@Autowired
	private IReservationDao rd;

	private void correct(long id){
		rd.correct(id);
	}
	
	public Reservation getReservation(long id) {
		correct(id);
		return rd.getReservation(id);
		}

	public void createReservation(Reservation reservation) {
		rd.createReservation(reservation);
		correct(reservation.getId());
		
	}

	public void removeReservation(long id) {
		correct(id);
		rd.removeReservation(id);
		
	}

	public void updateReservation(long id, Reservation reservation) throws ParseException {
		correct(id);
		rd.updateReservation(id, reservation);	
	}
	
	public List<Reservation> getReservations() {
		return rd.getReservations();
		
	}
	
}
