package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import dao.IClientDao;
import entity.Client;


@Component
public class ClientService {
	@Autowired
	private IClientDao cd;


	public Client getClient(long id) {
		return cd.getClient(id);
	}

	public void createClient(Client client) {
		cd.createClient(client);

	}

	public void removeClient(long id) {
		cd.removeClient(id);

	}

	public void updateClient(long id,Client client) {
		cd.updateClient(id, client);
	}
	
	public List<Client> getClients() {
		return cd.getClients();
		
	}
}
