package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import dao.IAgencyDao;
import entity.Agency;

@Component
public class AgencyService {
	
	@Autowired
	private IAgencyDao ad;

	public Agency getAgency(long id) {
		return ad.getAgency(id);
	}

	public void createAgency(Agency agency) {
		ad.createAgency(agency);

	}

	public void removeAgency(long id) {
		ad.removeAgency(id);

	}

	public void updateAgency(long id, Agency agency) {
		ad.updateAgency(id, agency);
	}

	public List<Agency> getAgencies() {
		return ad.getAgencies();
	}
}
