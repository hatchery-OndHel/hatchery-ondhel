package service;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import dao.ReservationDao;
import entity.Reservation;

@Component
public class ServiceReservation {
	
	@Autowired
	private ReservationDao rd;

	public Reservation getReservation(long id) {
		rd.getReservation(id);
		return null;
	}

	public void createReservation(Reservation reservation) {
		rd.createReservation(reservation);
		
	}

	public void removeReservation(long id) {
		rd.removeReservation(id);
		
	}

	public void updateReservation(long id, Reservation reservation) throws ParseException {
		rd.updateReservation(id, reservation);	
	}
	
	
	
}
