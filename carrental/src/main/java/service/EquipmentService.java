package service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import dao.IEquipmentDao;
import entity.Equipment;

@Component
public class EquipmentService {
	@Autowired
	private IEquipmentDao ed;


	public Equipment getEquipment(long id) {
		return ed.getEquipment(id);
	}

	public void createEquipment(Equipment equipment) {
		ed.createEquipment(equipment);

	}

	public void removeEquipment(long id) {
		ed.removeEquipment(id);

	}

	public void updateEquipment(long id, Equipment equipment) {
		ed.updateEquipment(id, equipment);
	}
	
	public List<Equipment> getEquipments(){
		return ed.getEquipments();
	}
}