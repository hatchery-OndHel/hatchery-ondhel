package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import dao.ICarDao;
import entity.Car;

@Component
public class CarService {
	@Autowired
	private ICarDao cd;


	public Car getCar(long id) {
		return cd.getCar(id);
	}

	public void createCar(Car car) {
		cd.createCar(car);

	}

	public void removeCar(long id) {
		cd.removeCar(id);

	}

	public void updateCar(long id, Car car) {
		cd.updateCar(id, car);
	}

	public List<Car> getCars() {
		return cd.getCars();
		
	}
}
