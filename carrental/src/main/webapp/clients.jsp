<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@page
	import="entity.*, java.util.*"%>
<body>
<head>
<style>
body {
	background-color: #70B670;
}
hr {
display: block; height: 1px; border: 0; border-top: 1px solid #000; margin: 1em 0; padding: 0;
}
</style>
</head>
<div>
	<h2>List of Clients</h2>

	<table title="List Of Clients" border="1">
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Surname</th>
			<th>Birth number</th>
			<th>Phone number</th>
			<th>Email</th>
			<th>Reservations' IDs</th>
		</tr>

		<%
			List<Client> clients = (List) request.getAttribute("Clients");
			for (Client cl : clients) {
				out.println("<tr>");
				out.println("<td>" + cl.getId() + "</td>");
				out.println("<td>" + cl.getName() + "</td>");
				out.println("<td>" + cl.getSurname() + "</td>");
				out.println("<td>" + cl.getBirthNumber() + "</td>");
				out.println("<td>" + cl.getPhoneNumber() + "</td>");
				out.println("<td>" + cl.getEmail() + "</td>");
				out.print("<td>");
				List<Reservation> reser = cl.getReservation();
				for (Reservation r : reser){
					out.print(r.getId() + " ");
				}
				out.print("</td>");
				out.println("</tr>");
			}
		%>

	</table>

</div>
</body>