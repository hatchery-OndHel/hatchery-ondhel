<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@page
	import="entity.*, java.util.*"%>

<div>
	<h2>List of Persons</h2>

	<table title="List Of Persons" border="1">
		<tr>
			<th>Name</th>
			<th>Age</th>
		</tr>

		<%
			List<Car> cars = (List) request.getAttribute("Cars");
			for (Car car : cars) {
				out.println("<tr>");
				out.println("<td>" + car.getId() + "</td>");
				out.println("<td>" + car.getColor() + "</td>");
				out.println("</tr>");
			}
		%>

	</table>

	</ol>
</div>