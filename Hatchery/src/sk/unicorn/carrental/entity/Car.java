package sk.unicorn.carrental.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Car {
	@Id @GeneratedValue
	private long id;
	private String spz;
	private String type;
	private String model;
	private String color;
	@Column(name="price_per_day")
	private int pricePerDay;
	@ManyToMany 
	private Set<Equipment> equipments;
	@ManyToOne
	@JoinColumn(name="ID_Agency")
	private Agency agency;

	private boolean available = true;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSpz() {
		return spz;
	}
	public void setSpz(String spz) {
		this.spz = spz;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getPricePerDay() {
		return pricePerDay;
	}
	public void setPricePerDay(int pricePerDay) {
		this.pricePerDay = pricePerDay;
	}
	public Set<Equipment> getEquipments() {
		return equipments;
	}
	public void setEquipments(Set<Equipment> equipments) {
		this.equipments = equipments;
	}
	
	public boolean isAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}
	public Car(String spz, String type, String model, String color, int pricePerDay, Agency ag, Set<Equipment> equipments) {
		super();
		this.spz = spz;
		this.type = type;
		this.model = model;
		this.color = color;
		this.pricePerDay = pricePerDay;
		this.agency = ag;
		this.equipments = equipments;
	}
	
	public Car(){
		
	}
	public Agency getAgency() {
		return agency;
	}
	public void setAgency(Agency agency) {
		this.agency = agency;
	}
	
	
	
}
