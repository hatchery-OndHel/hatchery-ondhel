package sk.unicorn.carrental.entity;

import java.util.Set;

import javax.persistence.*;

@Entity
public class Client {

	@Id
	@GeneratedValue
	private long id;
	private String name;
	private String surname;
	@Column(name = "birth_number")
	private String birthNumber;
	@Column(name = "phone_number")
	private String phoneNumber;
	private String email;
	@OneToMany(mappedBy="client")
	private Set<Reservation> reservation;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getBirthNumber() {
		return birthNumber;
	}

	public void setBirthNumber(String birthNumber) {
		this.birthNumber = birthNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Client(String name, String surname, String birthNumber, String phoneNumber, String email,
			Set<Reservation> reservation) {
		super();
		this.name = name;
		this.surname = surname;
		this.birthNumber = birthNumber;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.reservation = reservation;
	}

	public Set<Reservation> getReservation() {
		return reservation;
	}

	public void setReservation(Set<Reservation> reservation) {
		this.reservation = reservation;
	}
	
	
}
