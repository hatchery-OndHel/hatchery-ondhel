package sk.unicorn.carrental.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class Reservation {
	@Id @GeneratedValue
	private long id;
	@ManyToOne
	@JoinColumn(name="ID_Client")
	private Client client;
	@ManyToOne
	@JoinColumn(name="ID_Car")
	private Car car;
	@Column(name = "start_place")
	private String startPlace;
	@Column(name = "return_place")
	private String returnPlace;
	private int duration;
	@Column(name = "start_date")
	private Date startDate;
	@Column(name = "return_date")
	private Date returnDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public String getStartPlace() {
		return startPlace;
	}

	public void setStartPlace(String startPlace) {
		this.startPlace = startPlace;
	}

	public String getReturnPlace() {
		return returnPlace;
	}

	public void setReturnPlace(String returnPlace) {
		this.returnPlace = returnPlace;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public Reservation( Client client, Car car, String startPlace, String returnPlace, int duration,
			Date startDate, Date returnDate) {
		super();
		this.client = client;
		this.car = car;
		this.startPlace = startPlace;
		this.returnPlace = returnPlace;
		this.duration = duration;
		this.startDate = startDate;
		this.returnDate = returnDate;
	}
	
	
}
